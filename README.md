### Test-project that implements following features:

- Page that lists "buyers" with search, available for staff.
- Similar page with listing of products
- Generating an unique promo-code for specific user and product
- Page for "buyers", when user can see all his promo-codes
