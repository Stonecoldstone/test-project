from django.conf.urls import url
from django.contrib.auth.views import LogoutView

from . import views

urlpatterns = [
    url(r'^$', views.LoginView.as_view(), name='login'),
    url(r'^logout/$', LogoutView.as_view(), name='logout'),
    url(r'^users/$', views.ListUsers.as_view(), name='users'),
    url(r'^products/(?P<user_id>\d+)/$', views.ListProducts.as_view(), name='products'),
    url(r'^codes/$', views.ListCodes.as_view(), name='discount_codes'),
    url(r'^discount_form/$', views.get_discount_form, name='discount_form'),
    url(r'^get_discount_code/$', views.get_discount_code, name='get_discount_code'),

]
