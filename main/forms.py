from datetime import date

from datetimewidget import widgets
from django import forms
from django.contrib.auth.forms import AuthenticationForm, UsernameField
from django.forms import ValidationError
from django.utils.translation import ugettext_lazy as _


class LoginForm(AuthenticationForm):
    username = UsernameField(
        max_length=254,
        widget=forms.TextInput(attrs={'autofocus': True, 'class': 'form-control',
                                      'required': True}),
    )
    password = forms.CharField(
        label=_("Password"),
        strip=False,
        widget=forms.PasswordInput(attrs={'class': 'form-control', 'required': True}),
    )


class SearchForm(forms.Form):
    query = forms.CharField(max_length=254, widget=forms.TextInput(attrs={'class': 'form-control',
                                                                          'placeholder': 'Find by name'}),
                            required=False)


class DiscountForm(forms.Form):
    discount_percents = forms.IntegerField(max_value=100, min_value=1, widget=forms.NumberInput(attrs={
        'class': 'form-control'
    }))
    expires = forms.DateField(widget=widgets.DateWidget(attrs={'class': 'form-control'},
                                                        usel10n=True, bootstrap_version=3),
                              required=False)
    product_id = forms.CharField(widget=forms.HiddenInput())
    user_id = forms.CharField(widget=forms.HiddenInput())

    def clean_expires(self):
        expires = self.cleaned_data.get('expires')
        if expires:
            if expires <= date.today():
                raise ValidationError(_('Expire date should be greater than current date.'),
                                      code='low_date')
        return expires
