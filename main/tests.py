from datetime import date

from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.exceptions import ObjectDoesNotExist
from django.test import TestCase

from .models import DiscountCode, Product


class TestLoginView(TestCase):
    fixtures = ['users.json']

    def test_staff_redirected_to_users_page(self):
        resp = self.client.post('', {'username': 'admin', 'password': '1234567qq'})
        self.assertRedirects(resp, '/users/')

    def test_buyer_redirected_to_code_page(self):
        resp = self.client.post('', {'username': 'a_buyer', 'password': '12345678'})
        self.assertRedirects(resp, '/codes/')


class TestListUsersView(TestCase):
    fixtures = ['users.json', 'main.json']

    def setUp(self):
        user = get_user_model().objects.get(username='admin')
        self.client.force_login(user)

    def test_not_authenticated_redirected(self):
        self.client.logout()
        resp = self.client.get('/users/')
        self.assertRedirects(resp, '/?next=/users/')

    def test_buyer_gets_forbidden(self):
        buyer = get_user_model().objects.get(username='a_buyer')
        self.client.force_login(buyer)
        resp = self.client.get('/users/')
        self.assertEqual(resp.status_code, 403)

    def test_returns_sorted(self):
        paginate_by = getattr(settings, 'PAGINATE_BY', 6)
        self.assertGreaterEqual(paginate_by, 6)
        resp = self.client.get('/users/?ordering=3')
        username_order = [
            '<User: a_buyer>', '<User: apTlbt>', '<User: aUjGHa>',
            '<User: c_buyer>', '<User: cbNXBW>', '<User: cDOtaz>'
        ]
        qs = resp.context['users_list'][:6]
        self.assertQuerysetEqual(qs, username_order)
        resp = self.client.get('/users/?ordering=2')
        registration_new_order = [
            '<User: mmKZdf>', '<User: dKbTqo>', '<User: cDOtaz>',
            '<User: ejEIra>', '<User: lOAlxU>', '<User: NRxsXC>'
        ]
        qs = resp.context['users_list'][:6]
        self.assertQuerysetEqual(qs, registration_new_order)
        resp = self.client.get('/users/?ordering=1')
        registration_old_order = [
            '<User: test_buyer>', '<User: a_buyer>', '<User: c_buyer>',
            '<User: g_buyer>', '<User: z_buyer>', '<User: zorro>'
        ]
        qs = resp.context['users_list'][:6]
        self.assertQuerysetEqual(qs, registration_old_order)

    def test_usernames_contain_search_query(self):
        resp = self.client.get('/users/?query=buyer')
        qs = resp.context['users_list']
        values = [
            '<User: a_buyer>', '<User: c_buyer>', '<User: g_buyer>',
            '<User: test_buyer>', '<User: z_buyer>'
        ]
        self.assertQuerysetEqual(qs, values)


class TestCustomDecorators(TestCase):
    fixtures = ['users.json']

    @classmethod
    def setUpTestData(cls):
        cls.staff = get_user_model().objects.get(username='admin')
        cls.buyer = get_user_model().objects.get(username='a_buyer')

    def test_ajax_required_decorator(self):
        self.client.force_login(self.staff)
        resp = self.client.get('/discount_form/')
        self.assertEqual(resp.status_code, 400)
        resp = self.client.get('/discount_form/', HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(resp.status_code, 200)

    def test_passes_test_decorator(self):
        resp = self.client.get('/discount_form/', HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(resp.status_code, 403)


class TestGetDiscountCodeView(TestCase):
    fixtures = ['users.json', 'main.json']

    @classmethod
    def setUpTestData(cls):
        cls.staff = get_user_model().objects.get(username='admin')

    def setUp(self):
        expire_date = date.today()
        expire_date = expire_date.replace(month=expire_date.month + 1)
        self.post_data = {
            'discount_percents': 20, 'product_id': '4', 'user_id': 4,
            'expires': expire_date
        }

    def test_get_rejected(self):
        resp = self.client.get('/get_discount_code/')
        self.assertEqual(resp.status_code, 403)

    def test_form_errors(self):
        self.client.force_login(self.staff)
        expire_date = date(year=2016, month=10, day=25)
        self.post_data['expires'] = expire_date
        resp = self.client.post('/get_discount_code/', self.post_data,
                                HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        error = 'Expire date should be greater than current date.'
        self.assertFormError(resp, 'form', 'expires', [error])

    def test_code_created_with_correct_data(self):
        self.client.force_login(self.staff)
        # make sure that product and user exist
        try:
            get_user_model().objects.get(id=self.post_data['user_id'])
            Product.objects.get(id=self.post_data['product_id'])
        except ObjectDoesNotExist:
            raise AssertionError('Objects with ids from post data don\'t exist.')
        resp = self.client.post('/get_discount_code/', self.post_data,
                                HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'main/discount_code.html')
        code = resp.context['code']
        code_exists = DiscountCode.objects.filter(code=code).exists()
        self.assertTrue(code_exists)

    def test_code_updated(self):
        self.client.force_login(self.staff)
        self.client.post('/get_discount_code/', self.post_data,
                         HTTP_X_REQUESTED_WITH='XMLHttpRequest')

        code_obj = DiscountCode.objects.filter(user__id=self.post_data['user_id'],
                                               product__id=self.post_data['product_id'])
        self.assertEqual(len(code_obj), 1)
        first_code = code_obj[0].code
        self.client.post('/get_discount_code/', self.post_data,
                         HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        code_obj = DiscountCode.objects.filter(user__id=self.post_data['user_id'],
                                               product__id=self.post_data['product_id'])
        self.assertEqual(len(code_obj), 1)
        self.assertNotEqual(first_code, code_obj[0].code)


class TestListProductsView(TestCase):
    fixtures = ['users.json', 'main.json']

    def setUp(self):
        user = get_user_model().objects.get(username='admin')
        self.client.force_login(user)

    def test_not_authenticated_redirected(self):
        self.client.logout()
        resp = self.client.get('/products/4/')
        self.assertRedirects(resp, '/?next=/products/4/')

    def test_buyer_gets_forbidden(self):
        buyer = get_user_model().objects.get(username='a_buyer')
        self.client.force_login(buyer)
        resp = self.client.get('/products/4/')
        self.assertEqual(resp.status_code, 403)

    def test_returns_sorted(self):
        paginate_by = getattr(settings, 'PAGINATE_BY', 6)
        self.assertGreaterEqual(paginate_by, 6)
        resp = self.client.get('/products/4/?ordering=3')
        name_order = [
            '<Product: aaAHTd>', '<Product: bXCJrE>', '<Product: DbDQPl>',
            '<Product: EjPnNo>', '<Product: FHCSxv>', '<Product: jFBVmQ>'
        ]
        qs = resp.context['products_list'][:6]
        self.assertQuerysetEqual(qs, name_order)
        resp = self.client.get('/products/4/?ordering=2')
        price_high_low = [
            '<Product: jFBVmQ>', '<Product: aaAHTd>', '<Product: nAjzay>',
            '<Product: pmeohO>', '<Product: DbDQPl>', '<Product: XjbtqJ>'
        ]
        qs = resp.context['products_list'][:6]
        self.assertQuerysetEqual(qs, price_high_low)
        resp = self.client.get('/products/4/?ordering=1')
        price_low_high = [
            '<Product: test product>', '<Product: mwxsVo>', '<Product: jIDYUi>',
            '<Product: wBYhZL>', '<Product: OMdAps>', '<Product: qlGDRg>'
        ]
        qs = resp.context['products_list'][:6]
        self.assertQuerysetEqual(qs, price_low_high)

    def test_names_contain_search_query(self):
        resp = self.client.get('/products/4/?query=a')
        qs = resp.context['products_list']
        values = [
            '<Product: aaAHTd>', '<Product: nAjzay>', '<Product: OMdAps>',
        ]
        self.assertQuerysetEqual(qs, values)


class TestCodesView(TestCase):
    fixtures = ['users.json', 'main.json']

    def setUp(self):
        user = get_user_model().objects.get(username='a_buyer')
        self.client.force_login(user)

    def test_not_authenticated_redirected(self):
        self.client.logout()
        resp = self.client.get('/codes/')
        self.assertRedirects(resp, '/?next=/codes/')

    def test_staff_gets_forbidden(self):
        staff = get_user_model().objects.get(username='admin')
        self.client.force_login(staff)
        resp = self.client.get('/codes/')
        self.assertEqual(resp.status_code, 403)

    def test_returns_sorted(self):
        paginate_by = getattr(settings, 'PAGINATE_BY', 6)
        self.assertGreaterEqual(paginate_by, 6)
        resp = self.client.get('/codes/?ordering=3')
        name_order = [
            '<DiscountCode: SYS3>', '<DiscountCode: CC7Z>', '<DiscountCode: DY5U>',
            '<DiscountCode: J3M3>', '<DiscountCode: 7J6N>', '<DiscountCode: 6YHC>'
        ]
        qs = resp.context['codes_list'][:6]
        self.assertQuerysetEqual(qs, name_order)
        resp = self.client.get('/codes/?ordering=2')
        expires = [
            '<DiscountCode: NLLL>', '<DiscountCode: 7DNC>', '<DiscountCode: 3WB9>',
            '<DiscountCode: B9GQ>', '<DiscountCode: HB2Z>', '<DiscountCode: 7T4U>',
        ]
        qs = resp.context['codes_list'][:6]
        self.assertQuerysetEqual(qs, expires)
        resp = self.client.get('/codes/?ordering=1')
        discount_percents = [
            '<DiscountCode: 3WB9>', '<DiscountCode: CC7Z>', '<DiscountCode: LOXD>',
            '<DiscountCode: K5FA>', '<DiscountCode: B9GQ>', '<DiscountCode: WDQP>'
        ]
        qs = resp.context['codes_list'][:6]
        self.assertQuerysetEqual(qs, discount_percents)

    def test_names_contain_search_query(self):
        resp = self.client.get('/codes/?query=a')
        qs = resp.context['codes_list']
        values = [
            '<DiscountCode: SYS3>', '<DiscountCode: 3WB9>', '<DiscountCode: B9GQ>',
        ]
        self.assertQuerysetEqual(qs, values)
