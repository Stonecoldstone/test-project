from datetime import date, timedelta
from functools import wraps

from django.core.exceptions import PermissionDenied
from django.http import HttpResponseBadRequest


def set_default_date():
    return date.today() + timedelta(days=30)


def ajax_required(f):
    @wraps(f)
    def wrap(request, *args, **kwargs):
            if not request.is_ajax():
                return HttpResponseBadRequest()
            return f(request, *args, **kwargs)
    return wrap


def check_is_staff(user):
    return user.is_staff

def user_passes_test_custom(test_func):
    def decorator(view_func):
        @wraps(view_func)
        def _wrapped_view(request, *args, **kwargs):
            if test_func(request.user):
                return view_func(request, *args, **kwargs)
            raise PermissionDenied('Forbidden')
        return _wrapped_view
    return decorator


class UserPassesTestMixin:

    def test_func(self):
        raise NotImplementedError(
            '{0} is missing the implementation of the test_func() method.'.format(self.__class__.__name__)
        )

    def dispatch(self, request, *args, **kwargs):
        user_test_result = self.test_func()
        if not user_test_result:
            raise PermissionDenied('Forbidden')
        return super(UserPassesTestMixin, self).dispatch(request, *args, **kwargs)
