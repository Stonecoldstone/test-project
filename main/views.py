from datetime import date
from string import ascii_uppercase, digits

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView as AuthLoginView
from django.core.urlresolvers import reverse
from django.db.models.functions import Lower
from django.shortcuts import get_object_or_404, render
from django.utils.crypto import get_random_string
from django.views.decorators.http import require_http_methods
from django.views.generic.list import ListView

from . import forms
from .forms import DiscountForm, SearchForm
from .models import DiscountCode, Product
from .utils import (UserPassesTestMixin, ajax_required, check_is_staff,
                    set_default_date, user_passes_test_custom)


class LoginView(AuthLoginView):
    authentication_form = forms.LoginForm
    template_name = 'main/login.html'
    redirect_authenticated_user = True

    def get_success_url(self):
        url = self.get_redirect_url()
        if not url:
            user = self.request.user
            if user.is_staff:
                url = 'users'
            else:
                url = 'discount_codes'
            url = reverse(url)
        return url


class ListUsers(LoginRequiredMixin, UserPassesTestMixin, ListView):
    http_method_names = ['get']
    paginate_by = getattr(settings, 'PAGINATE_BY', 12)
    template_name = 'main/list_users.html'
    ordering_map = {
        '1': 'date_joined', '2': '-date_joined', '3': 'username'
    }
    context_object_name = 'users_list'
    search_field_name = 'query'

    def test_func(self):
        return self.request.user.is_staff

    def get_queryset(self):
        search_term = self.request.GET.get(self.search_field_name)
        queryset = get_user_model().objects.filter(is_staff=False)
        if search_term:
            queryset = queryset.filter(username__icontains=search_term)
        ordering = self.get_ordering()
        if ordering:
            if self.ordering_num == '3':
                ordering = Lower(ordering)
            queryset = queryset.order_by(ordering)
        return queryset

    def get_ordering(self):
        ordering = self.request.GET.get('ordering')
        if not ordering:
            ordering = '3'
        self.ordering_num = ordering
        ordering = self.ordering_map.get(ordering)
        return ordering

    def get_context_data(self):
        context = super().get_context_data()
        context['search_form'] = SearchForm()
        context['ordering'] = self.ordering_num
        return context


class ListProducts(ListUsers):
    template_name = 'main/list_products.html'
    ordering_map = {
        '1': 'price', '2': '-price', '3': 'name'
    }
    context_object_name = 'products_list'

    def get_queryset(self):
        search_term = self.request.GET.get(self.search_field_name)
        queryset = Product.objects.all()
        if search_term:
            queryset = queryset.filter(name__icontains=search_term)
        ordering = self.get_ordering()
        if ordering:
            if self.ordering_num == '3':
                ordering = Lower(ordering)
            queryset = queryset.order_by(ordering)
        return queryset

    def get_context_data(self):
        context = super().get_context_data()
        context['user_id'] = self.kwargs['user_id']
        return context


@user_passes_test_custom(check_is_staff)
@login_required
@ajax_required
def get_discount_form(request):
    form = DiscountForm(initial={'expires': set_default_date()})
    return render(request, 'main/discount_form.html', {'form': form})


@user_passes_test_custom(check_is_staff)
@login_required
@ajax_required
@require_http_methods(['POST'])
def get_discount_code(request):
    form = DiscountForm(request.POST)
    if form.is_valid():
        cd = form.cleaned_data
        user = get_object_or_404(get_user_model(), id=cd['user_id'])
        product = get_object_or_404(Product, id=cd['product_id'])
        retries = getattr(settings, 'GENERATE_CODE_TRIES', 20)
        DiscountCode.objects.filter(expires__lte=date.today()).delete()
        while True:
            code = get_random_string(4, ascii_uppercase + digits)
            if not DiscountCode.objects.filter(code=code).exists():
                defaults = {'code': code, 'discount_percents': cd['discount_percents']}
                expires = cd['expires']
                if expires:
                    defaults['expires'] = expires
                code_object, _ = DiscountCode.objects.update_or_create(user=user,
                                                                       product=product,
                                                                       defaults=defaults)
                return render(request, 'main/discount_code.html', {'code': code})
            if retries is 0:
                form.add_error(None, 'Cannot generate discount code.'
                                     ' Max number of retries is reached.')
                break
            retries -= 1
    return render(request, 'main/discount_form.html', {'form': form})


class ListCodes(ListUsers):
    template_name = 'main/list_codes.html'
    ordering_map = {
        '1': '-discount_percents', '2': 'expires', '3': 'product__name'
    }
    context_object_name = 'codes_list'

    def test_func(self):
        return not self.request.user.is_staff

    def get_queryset(self):
        search_term = self.request.GET.get(self.search_field_name)
        queryset = DiscountCode.objects.filter(user__id=self.request.user.id).select_related('product')
        if search_term:
            queryset = queryset.filter(product__name__icontains=search_term)
        ordering = self.get_ordering()
        if ordering:
            if self.ordering_num == '3':
                ordering = Lower(ordering)
            queryset = queryset.order_by(ordering)
        return queryset
