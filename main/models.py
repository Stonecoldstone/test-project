from decimal import Decimal

from django.conf import settings
from django.db import models

from .utils import set_default_date

# def set_default_date():
#     return date.today() + timedelta(days=30)


class Product(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField(max_length=4096, blank=True, null=True)
    price = models.DecimalField(max_digits=9, decimal_places=2)
    # image = models.ImageField()

    def __str__(self):
        return self.name

    class Meta:
        unique_together = ('name', 'price')


class DiscountCode(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE,
                             related_name='discount_codes')
    product = models.ForeignKey(Product, on_delete=models.CASCADE,
                                related_name='discount_codes')
    code = models.CharField(max_length=4, unique=True)
    discount_percents = models.IntegerField()
    created = models.DateField(auto_now_add=True)
    expires = models.DateField(default=set_default_date)

    def __str__(self):
        return self.code

    def calculate_price_after_discount(self):
        price = self.product.price
        result_price = price - Decimal(price * self.discount_percents / 100).\
            quantize(Decimal('.01'))
        return result_price
